<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 10; $i++) {
            $hotel = $this->generateAFakeHotel($i, $manager);
            $manager->persist($hotel);
        }

        $manager->flush();
    }

    private function generateAFakeHotel(int $index, ObjectManager $manager) : Hotel
    {
        $hotel = new Hotel();
        $hotel->setName("Hotel #{$index}");

        $reviewsAmount = random_int(10, 20);

        for($i = 0; $i < $reviewsAmount; $i++) {
            $review = $this->generateAFakeReview();
            $manager->persist($review);
            $hotel->addReview($review);
        }

        return $hotel;
    }

    private function generateAFakeReview() : Review
    {
        $review = new Review();
        $review->setComment("Lorem ipsum");
        $review->setScore(random_int(1, 5));
        $review->setCreated($this->generateAFakeDateBetween(new \DateTime('2019-01-01'), new \DateTime()));

        return $review;
    }

    private function generateAFakeDateBetween(\DateTime $startDate, \DateTime $endDate): \DateTime
    {
        $min = $startDate->getTimestamp();
        $max = $endDate->getTimestamp();
        $val = mt_rand($min, $max);

        $result = new \DateTime();
        $result->setTimestamp($val);

        return $result;
    }
}
