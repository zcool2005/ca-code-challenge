FROM library/php:8-apache

RUN apt update && apt-get install -y libpq-dev \
    git sqlite3

RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pdo_pgsql

# install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN chmod +x /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN a2enmod rewrite

COPY deployment/local/apache.conf /etc/apache2/sites-available/000-default.conf

