# CA - Code challenge

## Set up
```
git clone https://gitlab.com/zcool2005/ca-code-challenge.git
cd ca-code-challenge
docker-compose up -d
docker-compose exec php bash
composer install
./bin/console d:m:m
./bin/console d:f:l
```
Now the API should be available [here](http://localhost:8080/overtime/1?startDate=2020-01-01&endDate=2020-12-30)

## Tests
```
docker-compose up -d
docker-compose exec php bash
./bin/console -e test d:d:c 
./bin/console -e test d:m:m
./bin/phpunit 
```


