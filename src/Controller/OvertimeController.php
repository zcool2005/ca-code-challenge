<?php

namespace App\Controller;

use App\Entity\Hotel;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Services\OvertimeCalculatorService;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class OvertimeController extends AbstractFOSRestController
{
    private OvertimeCalculatorService $overtimeCalculatorService;

    public function __construct(OvertimeCalculatorService  $overtimeCalculatorService)
    {
        $this->overtimeCalculatorService = $overtimeCalculatorService;
    }


    /**
     * @Route("/overtime/{id}")
     * @QueryParam(name="startDate", nullable=false)
     * @QueryParam(name="endDate", nullable=false)
     */
    public function overtime(Hotel $hotel, ParamFetcherInterface $paramFetcher)
    {
        $results = $this->overtimeCalculatorService->calculate(
            $hotel,
            new \DateTime($paramFetcher->get('startDate')),
            new \DateTime($paramFetcher->get('endDate'))
        );

        return $this->handleView($this->view($results, 200));
    }
}