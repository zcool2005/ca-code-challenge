<?php

namespace App\Tests\Functional;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Tests\DatabasePrimer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OvertimeControllerTest extends WebTestCase
{
    private $client;
    private $hotel;

    public function setUp(): void
    {
        $this->client = static::createClient();
        DatabasePrimer::prime(self::$kernel);
        $this->hotel = $this->setUpTestingData();
    }

    public function testGroupingByDays()
    {
        $this->client->request(
            'GET',
            "/overtime/{$this->hotel->getId()}?startDate=2019-01-02&endDate=2019-01-05"
        );

        $response = $this->client->getResponse();

        $this->assertEquals($response->getStatusCode(), 200);
        $jsonResponse = json_decode($response->getContent(), true);

        $this->assertEquals($jsonResponse[0]['review-count'], 1);
        $this->assertEquals($jsonResponse[0]['average-score'], 5);
        $this->assertEquals($jsonResponse[0]['date-group'], 2);
        $this->assertEquals($jsonResponse[1]['review-count'], 1);
        $this->assertEquals($jsonResponse[1]['average-score'], 5);
        $this->assertEquals($jsonResponse[1]['date-group'], 3);
    }

    private function setUpTestingData()
    {
        $entityManager = $this->client
                              ->getContainer()
                              ->get('doctrine.orm.entity_manager');

        $hotel = new Hotel();
        $hotel->setName('Test hotel');
        $entityManager->persist($hotel);

        for($i = 1; $i <= 3; $i++)
        {
            $review = new Review();
            $review->setScore(5);
            $review->setComment('Test comment');
            $review->setCreated(new \DateTime("2019-01-0{$i}"));
            $review->setHotel($hotel);
            $entityManager->persist($review);
        }
        $entityManager->flush();

        return $hotel;
    }
}