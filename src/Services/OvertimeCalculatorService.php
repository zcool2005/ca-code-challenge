<?php


namespace App\Services;


use App\Dto\AverageScoreDto;
use App\Entity\Hotel;
use App\Repository\ReviewRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

class OvertimeCalculatorService
{
    private ReviewRepository $reviewRepository;

    public function  __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    public function calculate(Hotel $hotel, \DateTime $startDate, \DateTime $endDate): array
    {
        $groupingKind = $this->getGroupingKindByDateRange($startDate, $endDate);

        return $this->reviewRepository->getOvertimeAverageScore($groupingKind, $startDate, $endDate, $hotel);
    }

    private function getGroupingKindByDateRange(\DateTime $startDate, \DateTime $endDate): string
    {
        $dateDiff = (int)$endDate->diff($startDate, true)->format('a');

        if ($dateDiff > 89) {
            return 'MONTH';
        } else if ($dateDiff <= 89 && $dateDiff > 30) {
            return 'WEEK';
        }

        return 'DAY';
    }
}
