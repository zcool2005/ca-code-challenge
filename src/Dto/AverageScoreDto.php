<?php


namespace App\Dto;

use Symfony\Component\Serializer\Annotation\SerializedName;

class AverageScoreDto
{
    /** @SerializedName("review-count") */
    private int $reviewsAmount;

    /** @SerializedName("average-score") */
    private float $averageScore;

    /** @SerializedName("date-group") */
    private string $dateGroup;

    public function __construct(int $reviewsAmount, float $averageScore, string $dateGroup)
    {
        $this->reviewsAmount = $reviewsAmount;
        $this->averageScore = $averageScore;
        $this->dateGroup = $dateGroup;
    }

    public function getReviewsAmount(): int
    {
        return $this->reviewsAmount;
    }

    public function getAverageScore(): float
    {
        return $this->averageScore;
    }

    public function getDateGroup(): string
    {
        return $this->dateGroup;
    }
}