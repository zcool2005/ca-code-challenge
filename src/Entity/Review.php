<?php

namespace App\Entity;

use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 * @ORM\Table(indexes={@Index(name="created_idx", columns={"created"})})
 */
class Review
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
    * @Gedmo\Timestampable(on="create")
    * @ORM\Column(name="created", type="datetime")
    */
    private $created;

    /**
     * @ORM\ManyToOne(
     *     targetEntity=Hotel::class,
     *     inversedBy="reviews",
     *     cascade={"persist"}
     *  )
     * @ORM\JoinColumn(nullable=false)
     */
    private $hotel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getHotel(): ?Hotel
    {
        return $this->hotel;
    }

    public function setHotel(?Hotel $hotel): self
    {
        $this->hotel = $hotel;

        return $this;
    }

    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime  $created) : self
    {
        $this->created = $created;

        return $this;
    }
}
