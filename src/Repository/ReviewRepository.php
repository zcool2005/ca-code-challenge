<?php

namespace App\Repository;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Dto\AverageScoreDto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    public function getOvertimeAverageScore(string $grouping, \DateTime $startDate, \DateTime $endDate, Hotel $hotel): array
    {
        $result = $this->createQueryBuilder('r')
                       ->select("COUNT(r.id) as amount, AVG(r.score) as average, EXTRACT({$grouping} FROM r.created) as day_group")
                       ->where('r.created BETWEEN :startDate AND :endDate AND r.hotel = :hotel')
                       ->groupBy("day_group")
                       ->setParameter('startDate', $startDate->format('Y-m-d'))
                       ->setParameter('endDate', $endDate->format('Y-m-d'))
                       ->setParameter('hotel', $hotel->getId())
                       ->getQuery()
                       ->getResult();

        return array_map(function($item) {
            return new AverageScoreDto(
                $item['amount'],
                $item['average'],
                $item['day_group']
            );
        }, $result);
    }
}
